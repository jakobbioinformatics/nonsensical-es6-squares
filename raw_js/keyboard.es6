class Keyboard {

    // Allows the user to specify keycodes that are evaluated as pressed or not pressed
    // The user is able to get the status of a particular key

    constructor() {
        this.keys = {};
        this._setup_default_keys();
    }

    _setup_default_keys() {
        this.keys['left'] = new Key(37);
        this.keys['up'] = new Key(38);
        this.keys['right'] = new Key(39);
        this.keys['down'] = new Key(40);
        this.keys['space'] = new Key(32);
    }

    addKey(keycode) {
        this.keys[keycode] = new Key(keycode);
    }

    isKeyPressed(keycode) {
        return this.keys[keycode].isDown;
    }
}

class Key {

    // Represents a single key
    // Registers the given keycode, and tracks if it is pressed or not

    constructor(keycode) {
        let that = this;
        this.code = keycode;
        this.isDown = false;
        this.isUp = true;
        this.press = undefined;
        this.release = undefined;

        this.downHandler = function(event) {

            if (event.keyCode === that.code) {

                if (that.isUp) {
                    that.isDown = true;
                    that.isUp = false;
                }
            }
            event.preventDefault();
        };

        this.upHandler = function (event) {
            if (event.keyCode === that.code) {
                that.isDown = false;
                that.isUp = true;
            }
            event.preventDefault();
        };

        //Attach event listeners
        window.addEventListener(
            "keydown", this.downHandler.bind(this.key), false
        );
        window.addEventListener(
            "keyup", this.upHandler.bind(this.key), false
        );
    }
}

