// ------ Classes ------ //

class GameObject {

    constructor(texture, xpos, ypos) {

        this.sprite = new PIXI.Sprite(texture);

        this.x = xpos;
        this.y = ypos;

        this.is_dead = false;
        this.logging = false;
        this.movement = undefined;
    }

    get_sprite() { return this.sprite; }

    get x() { return this.sprite.position.x; };

    set x(value) { this.sprite.position.x = value; };

    get y() { return this.sprite.position.y; };

    set y(value) { this.sprite.position.y = value; };

    update(game_time) {

        if (this.movement != undefined) {
            this.movement.update(game_time);
        }
    }
}

class Square extends GameObject {

    constructor() {

        let texture = SQUARE_TEXT;
        let sprite = new PIXI.Sprite(texture);
        let x = Math.random() * LEVEL_WIDTH;
        let y = Math.random() * LEVEL_HEIGHT;

        super(texture, x, y);
        this.movement = new RandomBounceMovement(this);
    }

    update(game_time) {
        super.update(game_time);
    }

    explode() {
        this._generate_explosion(this.x, this.y);
        EXPLOSION_SFXR.play();
    }

    _generate_explosion(xcenter, ycenter) {
        let texture = PARTICLE_TEXT;
        let nbr_particles = 50;
        for (let i = 0; i < nbr_particles; i++) {
            game_objects.push(new Particle(xcenter, ycenter));
        }
    }
}

class Particle extends GameObject {

    constructor(x, y) {

        let speed = 5;
        super(PARTICLE_TEXT, x, y, speed);
        this.life_cycles = 10;
        super.get_sprite().tint = Math.random() * 0xFFFFFF;
    }

    update(game_time) {

        super.update(game_time);
        if (this.life_cycles > 0) {
            this.life_cycles--;
        }
        else {
            this.is_dead = true;
        }
    }
}

class Player extends GameObject {

    constructor(x, y) {
        super(SQUARE_TEXT, x, y);
        super.get_sprite().tint = 0xFF0000;
        this.movement = new PlayerMovement(this);
    }

    update(game_time) {
        super.update(game_time);
   }
}