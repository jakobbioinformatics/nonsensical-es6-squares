console.log('HELLOOO!!');

let stage = new PIXI.Stage(0x66FF99);
let renderer;
let game_objects = [];

let SQUARE_TEXT = PIXI.Texture.fromImage("img/green_square.bmp");
let PARTICLE_TEXT = PIXI.Texture.fromImage("img/small_white_square.png");
let EXPLOSION_SFXR = new Howl({ urls: ['audio/light_explosion.wav'] });

//let text_test = new PIXI.Text("Pixi.js can has text", {font:"50px Arial", fill:"white"});

const LEVEL_WIDTH = 512;
const LEVEL_HEIGHT = 384;
let stage_manager = new StageManagerInstance_temp();

function init() {

    //stage.addChild(text_test);

    renderer = PIXI.autoDetectRenderer(LEVEL_WIDTH, LEVEL_HEIGHT, {view:document.getElementById("game-canvas")});
    requestAnimationFrame(update);
}

function update() {

    let game_time = new Date().getTime() / 1000;
    renderer.render(stage);
    requestAnimationFrame(update);
    stage_manager.update(game_time);
}


