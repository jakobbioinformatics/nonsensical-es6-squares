class StageManagerInstance_temp {

    constructor() {

        this.stages = {};
        this.stages['menu'] = new MenuState();
        this.stages['level'] = new LevelState();

        this.current_stage = this.stages['menu'];
        this.current_stage.initialize();
    }

    change_state(stage_key) {
        this.current_stage.deinitialize();
        this.current_stage = this.stages[stage_key];
        this.current_stage.initialize();
    }

    update(game_time) {
        this.current_stage.update(game_time);
    }
}

class State {

    constructor(){
        this.game_objects = [];
        this.background_objects = [];
    }

    initialize() {

    }

    deinitialize() {

        // Game objects
        for (let i = 0; i < this.game_objects.length; i++) {
            console.log(this.game_objects[i]);
            stage.removeChild(this.game_objects[i].get_sprite());
        }
        this.game_objects = [];

        // Background objects
        for (let i = 0; i < this.background_objects.length; i++) {
            stage.removeChild(this.background_objects[i]);
        }
        this.background_objects = [];
    }

    update(game_time) {

    }

    add_game_object(obj) {
        this.game_objects.push(obj);
        stage.addChild(obj.get_sprite());
    }

    add_background_object(obj) {
        this.background_objects.push(obj);
        stage.addChild(obj);
    }
}

class LevelState extends State {

    constructor() {
        super();

        this.create_timing = 0;
        this.create_delay = 1.5;
        this.CREATE_DELAY_DECAY = 0.9;

        this.TOTAL_TIME = 25;
    }

    initialize() {
        super.initialize();
        this.is_game_over = false;
        this.end_time = new Date().getTime() / 1000 + this.TOTAL_TIME;
        this._spawn_initial_setup();
    }

    _spawn_initial_setup() {
        this.add_game_object(new Player(200, 200));
        this.add_game_object(new Square());
        this.add_game_object(new Square());
        this.add_game_object(new Square());
    }

    update(game_time) {
        super.update(game_time);

        if (!this.is_game_over) {
            this._spawn_square(game_time);
        }

        this._check_game_over(game_time);

        this.game_objects.forEach(function(obj) {
            obj.update(game_time);
        });

        this._remove_the_dead();
    }

    _check_game_over(game_time) {
        if (game_time > this.end_time && !this.is_game_over) {
            this.game_objects.forEach(function(obj) {
                if (obj instanceof Square) {
                    obj.explode();
                    obj.is_dead = true;
                }
            });
            this.is_game_over = true;
        }
    }

    _spawn_square(game_time) {

        if (game_time >= this.create_timing) {
            this.game_objects.push(new Square(SQUARE_TEXT, 100, 100));
            this.create_timing = game_time + this.create_delay;
            this.create_delay *= this.CREATE_DELAY_DECAY;
        }
    }

    _remove_the_dead() {
        let marked_by_death_array = [];

        for (let i = 0; i < this.game_objects.length; i++) {
            if (this.game_objects[i].is_dead) {
                marked_by_death_array.push(this.game_objects[i]);
            }
        }

        for (let i = 0; i < marked_by_death_array.length; i++) {
            stage.removeChild(this.game_objects[i].get_sprite());
            this.game_objects.splice(i, 1);
        }
    }
}

class MenuState extends State {

    constructor() {
        super();
        this.keyboard = new Keyboard();
    }

    initialize() {
        super.initialize();

        let text = new PIXI.Text("This is a menu\nPress Space", {font:"50px Arial", fill:"white"});
        this.add_background_object(text);
    }

    update(game_time) {
        super.update(game_time);
        if (this.keyboard.isKeyPressed('space')) {
            alert('space pressed!');
            stage_manager.change_state('level');
        }
    }
}










