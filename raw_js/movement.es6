var Direction = {
    'LEFT': 1,
    'UP': 2,
    'RIGHT': 3,
    'DOWN': 4,
    'NONE': 0
};

class Movement {

    constructor(game_object) {
        this.obj = game_object;
        this.speed = 0;
        this.directionx = 0;
        this.directiony = 0;
    }

    update(game_time) {
        this.obj.x += this.speed * this.directionx;
        this.obj.y += this.speed * this.directiony;
    }

    get_edge_collision() {

        if (this.obj.x < 0) {
            return Direction.LEFT;
        }
        else if (this.obj.x + this.obj.sprite.width > LEVEL_WIDTH) {
            return Direction.RIGHT;
        }
        else if (this.obj.y < 0) {
            return Direction.UP;
        }
        else if (this.obj.y + this.obj.sprite.height > LEVEL_HEIGHT) {
            return Direction.DOWN;
        }
        else {
            return Direction.NONE;
        }
    }
}

class RandomBounceMovement extends Movement {

    constructor(game_object) {
        super(game_object);

        this.speed = 2;
        this.directionx = 2 * Math.random() * this.speed - this.speed;
        this.directiony = 2 * Math.random() * this.speed - this.speed;
    }

    update(game_time) {
        super.update(game_time);
        this._edge_bounce();
    }

    _edge_bounce() {
        let edge_collision = super.get_edge_collision();

        switch (edge_collision) {
            case Direction.LEFT: {
                this.directionx = Math.abs(this.directionx);
                break;
            }
            case Direction.UP: {
                this.directiony = Math.abs(this.directiony);
                break;
            }
            case Direction.RIGHT: {
                this.directionx = -Math.abs(this.directionx);
                break;
            }
            case Direction.DOWN: {
                this.directiony = -Math.abs(this.directiony);
                break;
            }
            default: {
                break;
            }
        }
    }
}

class PlayerMovement extends Movement {

    constructor (game_object) {
        super(game_object);

        this.keyboard = new Keyboard();
    }

    update(game_time) {
        super.update(game_time);
        this._update_keyboard_controls();
    }

    _update_keyboard_controls() {

        let edgeCollision = super.get_edge_collision();

        let hasMoved = false;
        this.directionx = 0;
        this.directiony = 0;

        if (this.keyboard.isKeyPressed('left') && !(edgeCollision === Direction.LEFT)) {
            this.directionx = -1;
            hasMoved = true;
        }
        else if (this.keyboard.isKeyPressed('right') && !(edgeCollision === Direction.RIGHT)) {
            this.directionx = 1;
            hasMoved = true;
        }

        if (this.keyboard.isKeyPressed('up') && !(edgeCollision === Direction.UP)) {
            this.directiony = -1;
            hasMoved = true;
        }
        else if (this.keyboard.isKeyPressed('down') && !(edgeCollision === Direction.DOWN)) {
            this.directiony = 1;
            hasMoved = true;
        }

        if (hasMoved) {
            this.speed = 5;
        }
        else {
            this.speed = 0;
        }
    }
}